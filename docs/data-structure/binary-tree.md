# 二叉树

## 二叉树的递归遍历

## 二叉树的迭代遍历

### 144.二叉树的前序遍历

https://leetcode.cn/problems/binary-tree-preorder-traversal/

### 94.二叉树的中序遍历

https://leetcode.cn/problems/binary-tree-inorder-traversal/

### 145.二叉树的后序遍历

https://leetcode.cn/problems/binary-tree-postorder-traversal/


## 二叉树的统一迭代法


## 二叉树层序遍历

### 102.二叉树的层序遍历

https://leetcode.cn/problems/binary-tree-level-order-traversal/

### 107.二叉树的层次遍历II

https://leetcode.cn/problems/binary-tree-level-order-traversal-ii/

### 199.二叉树的右视图

https://leetcode.cn/problems/binary-tree-right-side-view/

### 637.二叉树的层平均值

https://leetcode.cn/problems/average-of-levels-in-binary-tree/

### 429.N叉树的层序遍历

https://leetcode.cn/problems/n-ary-tree-level-order-traversal/

### 515.在每个树行中找最大值

https://leetcode.cn/problems/find-largest-value-in-each-tree-row/

### 116.填充每个节点的下一个右侧节点指针

https://leetcode.cn/problems/populating-next-right-pointers-in-each-node/

### 117.填充每个节点的下一个右侧节点指针II

https://leetcode.cn/problems/populating-next-right-pointers-in-each-node-ii/

### 104.二叉树的最大深度

https://leetcode.cn/problems/maximum-depth-of-binary-tree/

### 111.二叉树的最小深度

https://leetcode.cn/problems/minimum-depth-of-binary-tree/

## 226.翻转二叉树

https://leetcode.cn/problems/invert-binary-tree/

## 101.对称二叉树

https://leetcode.cn/problems/symmetric-tree/

## 104.二叉树的最大深度

https://leetcode.cn/problems/maximum-depth-of-binary-tree/

## 111.二叉树的最小深度

https://leetcode.cn/problems/minimum-depth-of-binary-tree/

## 222.完全二叉树的节点个数

https://leetcode.cn/problems/count-complete-tree-nodes/

## 110.平衡二叉树

https://leetcode.cn/problems/balanced-binary-tree/

## 257. 二叉树的所有路径

https://leetcode.cn/problems/binary-tree-paths/

## 404.左叶子之和

https://leetcode.cn/problems/sum-of-left-leaves/

## 513.找树左下角的值

https://leetcode.cn/problems/find-bottom-left-tree-value/

## 112.路径总和

https://leetcode.cn/problems/path-sum/

## 106.从中序与后序遍历序列构造二叉树   

https://leetcode.cn/problems/construct-binary-tree-from-inorder-and-postorder-traversal/

## 654.最大二叉树

https://leetcode.cn/problems/maximum-binary-tree/

## 617.合并二叉树

https://leetcode.cn/problems/merge-two-binary-trees/

## 700.二叉搜索树中的搜索

https://leetcode.cn/problems/search-in-a-binary-search-tree/

## 98.验证二叉搜索树

https://leetcode.cn/problems/validate-binary-search-tree/

## 530.二叉搜索树的最小绝对差

https://leetcode.cn/problems/minimum-absolute-difference-in-bst/

## 501.二叉搜索树中的众数

https://leetcode.cn/problems/find-mode-in-binary-search-tree/

## 236. 二叉树的最近公共祖先

https://leetcode.cn/problems/lowest-common-ancestor-of-a-binary-tree/

## 235. 二叉搜索树的最近公共祖先

https://leetcode.cn/problems/lowest-common-ancestor-of-a-binary-search-tree/

## 701.二叉搜索树中的插入操作

https://leetcode.cn/problems/insert-into-a-binary-search-tree/

## 450.删除二叉搜索树中的节点

https://leetcode.cn/problems/delete-node-in-a-bst/

## 669. 修剪二叉搜索树

https://leetcode.cn/problems/trim-a-binary-search-tree/

## 108.将有序数组转换为二叉搜索树

https://leetcode.cn/problems/convert-sorted-array-to-binary-search-tree/

## 538.把二叉搜索树转换为累加树

https://leetcode.cn/problems/convert-bst-to-greater-tree/