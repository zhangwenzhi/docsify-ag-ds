# mkdocs

> MkDocs 是一个基于 Python 的静态网站生成器，用于构建文档网站。它使用 Markdown 语法，并且支持插件和主题。MkDocs 的目标是提供一种快速、简单的方式来创建文档网站。